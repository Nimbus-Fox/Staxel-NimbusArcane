﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.NimbusArcane.Components.Builders {
    public class SpellCircleTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "arcaneSpellCircleTile";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new SpellCircleTileComponent();
        }
    }
}