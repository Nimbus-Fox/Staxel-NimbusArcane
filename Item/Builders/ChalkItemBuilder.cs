﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.Interfaces;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.NimbusArcane.Item.Builders {
    public class ChalkItemBuilder : IItemBuilderHelperV1 {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();

        }
        public Staxel.Items.Item Build(Blob blob, ItemConfiguration configuration, Staxel.Items.Item spare) {
            if (spare is ChalkItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var chalk = new ChalkItem(this);
            chalk.Restore(configuration, blob);
            return chalk;

        }

        public static string KindCode() {
            return "nimbusFox.nimbusArcane.item.chalk";
        }

        public string Kind() {
            return KindCode();
        }

        public ItemRenderer Renderer { get; private set; }
    }
}
