﻿using System.Linq;
using NimbusFox.NimbusArcane.Components;
using NimbusFox.NimbusArcane.Effects.Builders;
using NimbusFox.NimbusArcane.Entities.SpellCircle;
using NimbusFox.NimbusArcane.Item.Builders;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.NimbusArcane.Item {
    public class ChalkItem : Staxel.Items.Item {
        private ChalkItemBuilder _builder;
        public string SpellCircle { get; private set; }
        protected bool NeedsStorage { get; private set; } = true;

        public ChalkItem(ChalkItemBuilder builder) : base(builder.Kind()) {
            _builder = builder;
        }
        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (!main.DownClick && !alt.DownClick || entity.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (main.DownClick) {
                if (entity.PlayerEntityLogic.LookingAtTile(out var lookAt, out var adjacent)) {
                    var vec = new Vector3I(5, 1, 5);

                    var tiles = new Tile[25];

                    if (facade.ReadTileRegion(lookAt + new Vector3I(-2, 0, -2), vec, tiles, TileAccessFlags.SynchronousWait,
                            ChunkFetchKind.LivingWorld, entity.Id)) {
                        if (tiles.All(x => x.Configuration.Components.Contains<SpellCircleTileComponent>())) {
                            if (!entity.PlayerEntityLogic.CreativeModeEnabled()) {
                                var stack = entity.Inventory.ActiveItem();
                                stack.Count = 1;
                                entity.Inventory.RemoveItem(stack);
                            }

                            SpellCircleEntityBuilder.Spawn(adjacent, facade, SpellCircle, GetItemCode());
                        } else {
                            var blb = BlobAllocator.Blob(true);
                            blb.SetString("code", "nimbusFox.nimbusArcane.notifications.invalidSpellCircleTile");
                            
                            entity.PlayerEntityLogic.ShowNotification(GameContext.NotificationDatabase.CreateNotification(blb, facade.Step, NotificationParams.EmptyParams, true));
                            
                            Blob.Deallocate(ref blb);
                        }
                    }
                }
            }
        }

        protected override void AssignFrom(Staxel.Items.Item item) { }
        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            if (NeedsStorage) {
                blob.SetString("spellCircle", SpellCircle);
                NeedsStorage = false;
            }
        }

        public override void StorePersistenceData(Blob blob) {
            base.StorePersistenceData(blob);
            blob.SetString("spellCircle", SpellCircle);
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);
            Configuration = configuration;

            SpellCircle = blob.GetString("spellCircle", "nimbusfox.nimbusArcane.spellCircle.transmutation");
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override bool TryResolveMainInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            verb = "nimbusfox.nimbusArcane.verb.Draw";

            return true;
        }

        public override bool HasAltAction() {
            return true;
        }

        public override bool Same(Staxel.Items.Item item) {
            if (item.Kind == NullItem.Kind) {
                return false;
            }

            return item.GetItemCode() == GetItemCode();
        }

        public void NeedStore() {
            NeedsStorage = true;
        }
    }
}
