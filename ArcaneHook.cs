﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Interfaces;
using NimbusFox.KitsuneToolBox.ToolBox.V1;
using Plukit.Base;
using Staxel.Browser;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.NimbusArcane {
    internal class ArcaneHook : IToolBoxHookUI {

        internal static readonly ToolBox ToolBox;

        static ArcaneHook() {
            ModHelper.CheckModIsInstalled("NimbusArcane", "KitsuneToolBox");
            ToolBox = new ToolBox("NimbusFox", "NimbusArcane");
        }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() {
            ArcaneContext.Initialize();
        }

        public void GameContextDeinitialize() {
            ArcaneContext.Deinitialize();
        }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }

        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        public void LoadWebElements(string url, out string[] htmlFiles, out string[] stylesheetFiles, out string[] javascriptFiles) {
            var html = new List<string>();
            var css = new List<string>();
            var js = new List<string>();
            if (url == "asset://gameassets/overlay.html") {
                html.Add("mods/NimbusArcane/Staxel/UI/tomeOfMagic.html");
                css.Add("mods/NimbusArcane/Staxel/UI/css/nimbusArcane.css");
                js.Add("mods/NimbusArcane/Staxel/UI/js/nimbusArcane.js");
            }
        
            htmlFiles = html.ToArray();
            stylesheetFiles = css.ToArray();
            javascriptFiles = js.ToArray();
        }

        public void OnWebUILoad(string url, BrowserRenderSurface surface) {
            if (url == "asset://gameassets/overlay.html") {
                ArcaneContext.SpellCircleDatabase.SendCirclesToUI(surface);
            }
        }
    }
}
