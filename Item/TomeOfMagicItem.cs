﻿using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.NimbusArcane.Effects.Builders;
using NimbusFox.NimbusArcane.Item.Builders;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Effects;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.NimbusArcane.Item {
    public class TomeOfMagicItem : Staxel.Items.Item {
        private readonly TomeOfMagicItemBuilder _builder;
        private bool _needStore;
        private bool _isOpen;

        private ItemConfiguration _closed;
        private ItemConfiguration _open;

        public TomeOfMagicItem(TomeOfMagicItemBuilder builder) : base(builder.Kind()) {
            _builder = builder;
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) {
            if (_needStore) {
                entity.Inventory.ItemStoreNeedsStorage();
            }
        }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (alt.DownClick) {
                if (entity.Logic is PlayerEntityLogic playerLogic) {
                    var blob = BlobAllocator.Blob(true);
                    blob.SetLong("playerEntity", entity.Id.Id);
                    entity.Effects.Start(new EffectTrigger(ReadTomeOfMagicEffectBuilder.KindCode, blob));
                }
            }
        }

        protected override void AssignFrom(Staxel.Items.Item item) { }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe,
            Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public override bool HasAltAction() {
            return true;
        }

        public override bool TryResolveAltInteractVerb(Entity entity, EntityUniverseFacade facade, Vector3I location,
            TileConfiguration lookedAtTile, out string verb) {
            verb = "nimbusFox.nimbusArcane.verb.read";
            return true;
        }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override bool Same(Staxel.Items.Item item) {
            if (item.Kind == NullItem.Kind) {
                return false;
            }

            return item.GetItemCode().StartsWith(GetItemCode());
        }

        public override string GetItemCode() {
            return _closed?.Code ?? Configuration.Code;
        }

        public override void Restore(ItemConfiguration configuration, Blob blob) {
            base.Restore(configuration, blob);

            if (configuration.Code.EndsWith(".open")) {
                _open = configuration;
                if (GameContext.ItemDatabase.TryGetItemConfiguration(
                        configuration.Code.Substring(0, configuration.Code.Length - 5), out var config) &&
                    config is ItemConfiguration itemConfig) {
                    _closed = itemConfig;
                } else {
                    _closed = configuration;
                }
            } else {
                _closed = configuration;
                if (GameContext.ItemDatabase.TryGetItemConfiguration(configuration.Code + ".open", out var config) &&
                    config is ItemConfiguration itemConfig) {
                    _open = itemConfig;
                } else {
                    _open = configuration;
                }
            }

            if (blob.Contains("isOpen")) {
                _isOpen = blob.GetBool("isOpen");
            }

            Configuration = _isOpen ? _open : _closed;
        }

        public override void Store(Blob blob) {
            base.Store(blob);
            if (_needStore) {
                blob.SetBool("isOpen", _isOpen);

                _needStore = false;
            }
        }

        public override void StorePersistenceData(Blob blob) {
            Configuration = _closed;
            base.StorePersistenceData(blob);
        }

        public void CloseBook() {
            _isOpen = false;
            _needStore = true;
        }

        public void OpenBook() {
            _isOpen = true;
            _needStore = true;
        }
    }
}