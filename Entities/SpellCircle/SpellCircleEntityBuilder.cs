﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.NimbusArcane.Entities.SpellCircle {
    public class SpellCircleEntityBuilder : IEntityLogicBuilder2, IEntityPainterBuilder {
        public readonly static string KindCode = "nimbusfox.nimbusArcane.entity.spellcircle";

        public EntityLogic Instance(Entity entity, bool server) {
            return new SpellCircleEntityLogic(entity);
        }

        public EntityPainter Instance() {
            return new SpellCircleEntityPainter();
        }

        public void Load() { }

        public string Kind => KindCode;

        public bool IsTileStateEntityKind() {
            return false;
        }

        public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, [NotNull]string spellCircleCode, [NotNull] string itemCode) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.FetchBlob("velocity").SetVector3F(Vector3F.Zero);
            blob.SetString("spellCircle", spellCircleCode);
            blob.SetString("chalkCode", itemCode);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            return entity;
        }
    }
}
