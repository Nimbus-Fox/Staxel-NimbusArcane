﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;

namespace NimbusFox.NimbusArcane.Database.Item {
    public struct SpellCircle {
        public readonly string Code;
        public readonly string Texture;
        public readonly string RecipeStep;
        public readonly ReadOnlyCollection<ReactionDefinition> Reactions;

        public SpellCircle(Blob config) {
            Code = config.GetString("code");
            Texture = config.GetString("texture", "mods/NimbusArcane/Staxel/Textures/SpellCircle/PlaceHolder.png");
            RecipeStep = config.GetString("recipeStep");

            Reactions = new ReadOnlyCollection<ReactionDefinition>(GameContext.ReactionDatabase
                .GetAllReactionDefinitions().Values
                .Where(x => x.RecipeStep == config.GetString("recipeStep")).ToList());
        }
    }
}
