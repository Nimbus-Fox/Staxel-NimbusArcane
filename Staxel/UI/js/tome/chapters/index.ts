﻿import Front from "./front";
import SpellCircles from "./spellCircles";

export interface Chapter {
    readonly id: string;
    readonly langId: string;
    init: () => void;
    show: () => void;
    updateLang: () => void;
    showPageButtons: () => boolean;
    canTurnBack: () => boolean;
    canTurnForward: () => boolean;
    turnBack: () => void;
    turnForward: () => void;
}

module Chapters {
    let chapters: Array<Chapter> = [];
    
    export function getChapters() {
        return chapters;
    }
    
    export function addChapter(chapter: Chapter) {
        chapters.push(chapter);
    }

    Chapters.addChapter(new Front());
    Chapters.addChapter(new SpellCircles());
}

export default Chapters;

stxl["nimbusArcane"]["tomeChapters"] = Chapters;