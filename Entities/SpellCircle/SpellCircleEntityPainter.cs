﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Drawables;
using NimbusFox.KitsuneToolBox.Managers.V1;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.NimbusArcane.Entities.SpellCircle {
    public class SpellCircleEntityPainter : EntityPainter {
        private TextureDrawable _spellDrawable;
        private const int _size = 400;
        private const float _worldSize = 2f;
        private float _degrees;
        private string spellCircle;

        public SpellCircleEntityPainter() {
            _spellDrawable = new TextureDrawable(new Vector2(_size, _size),
                context => ContentManager.FetchTexture(context,
                    spellCircle));

        }

        protected override void Dispose(bool disposing) { }

        public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController,
            EntityUniverseFacade facade,
            int updateSteps) {
            _degrees += 0.1f;

            if (_degrees >= 360) {
                _degrees -= 360;
            }
        }

        public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }
        public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

        public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController,
            Timestep renderTimestep) {
            if (entity.Logic is SpellCircleEntityLogic logic) {
                spellCircle = logic.SpellCircle?.Texture;
            }
        }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
            AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {

            var mat = (Matrix.CreateFromYawPitchRoll(0, MathHelper.ToRadians(-90), 0)
                       * Matrix.CreateTranslation(0, _worldSize, _worldSize)
                       * Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(_degrees), 0, 0)
                       * Matrix.CreateTranslation(0, -_worldSize, -_worldSize)
                ).ToMatrix4F().Translate(entity.Physics.Position - renderOrigin);

            _spellDrawable.Render(graphics, ref mat);

            if (entity.Logic is SpellCircleEntityLogic logic) {
                var results = logic.GetResults();
                if (results == null) {
                    return;
                }

                if (results.Any()) {
                    graphics.PushShader();
                    graphics.SetStencilMode();
                    graphics.ClearStencil();
                    graphics.SetShader(graphics.GetShader("FatQuadMeshStipple"));
                    graphics.SetShaderValue("xZBias", -0.00015f);

                    var mat2 = Matrix4F.CreateRotationY(MathHelper.ToRadians(_degrees))
                        .Translate((logic.Origin.ToTileCenterVector3D() + new Vector3D(0, 1, 0)) - renderOrigin);

                    foreach (var result in results) {
                        result.Item.FetchRenderer().RenderInWorldFullSized(result.Item, graphics, ref mat2, false, false);
                    }

                    graphics.PopShader();
                }
                
            }
        }

        public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }
    }
}
