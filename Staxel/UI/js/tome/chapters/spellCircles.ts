﻿import {Chapter} from "./index";
import SpellCircle = NimbusArcane.SpellCircle;

class SpellCircles implements Chapter {
    private static circles: { [key: string]: SpellCircle } = {};
    private static selected: string;
    private menu = $("#SpellCirclesContainer");

    readonly id = "spellCircles";
    readonly langId = "nimbusFox.nimbusArcane.chapter.spellCircles";
    
    constructor() {
        
    }
    
    public init() {
        
    }
    
    public canTurnBack() {
        return false;
    }

    public canTurnForward() {
        return false;
    }

    public show() {
    }

    public showPageButtons() {
        return false;
    }

    public turnBack() {
    }

    public turnForward() {
    }

    public updateLang() {
    }

    public static addSpellCircle(input: string | SpellCircle) {
        try {
            let spellCircle: SpellCircle;
            
            if (typeof input === "string") {
                spellCircle = JSON.parse(input);
            } else if (typeof input === "object") {
                spellCircle = input;
            }

            if (spellCircle === undefined) {
                logError("Invalid spell circle json");
                return;
            }

            if (spellCircle.code === undefined || spellCircle.texture === undefined) {
                logError("Invalid spell circle json");
                return;
            }

            spellCircle.element = $("<a href='#'>");
            spellCircle.element.addClass("spellCircle");

            spellCircle.element.on("click", () => {
                this.selected = spellCircle.code;
            })

            spellCircle.imageElement = $(`<img src='asset://gameassets/${spellCircle.texture}' height="50px" width="50px" alt='Failed to fetch texture'/>`)

            spellCircle.textElement = $(`<div data-lang='${spellCircle.code}.name'>`).text(`${spellCircle.code}`);
            spellCircle.textElement.addClass("spellCircleText");

            spellCircle.element.append(spellCircle.imageElement, spellCircle.textElement);

            spellCircle.element.hide();

            this.circles[spellCircle.code] = spellCircle;

            $("#SpellCircles").append(spellCircle.element);

            getLanguageString(spellCircle.code + ".name");

            logDebug(`Got new spell circle ${spellCircle.code}`);
        } catch (ex) {
            logError("Invalid spell circle json");
            logError(ex.toString());
        }
    }

    private static setSelectedCircle(code: string) {
        if (!SpellCircles.circles.hasOwnProperty(code)) {
            return;
        }

        SpellCircles.circles[code].element.click();
    }

    public static showSpellCircle(input: string) {
        logDebug(`Spell circle ${input} was requested to show`);
        if (!SpellCircles.circles.hasOwnProperty(input)) {
            return;
        }

        SpellCircles.circles[input].element.show();
    }

    public static hideSpellCircle(input: string) {
        if (!SpellCircles.circles.hasOwnProperty(input)) {
            return;
        }

        SpellCircles.circles[input].element.hide();
    }
}

export default SpellCircles;

stxl["nimbusArcane"]["spellCircles"] = SpellCircles;