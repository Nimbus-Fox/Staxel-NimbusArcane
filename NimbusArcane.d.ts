﻿declare namespace stxl.nimbusArcane {
    namespace tomeOfMagic {
        const leftPage: JQuery;
        const rightPage: JQuery;

        function clear(): void;

        function showFrontPage(): void;

        function showChapter(key: string);
    }

    namespace spellCircles {
        function addSpellCircle(input: string | NimbusArcane.SpellCircle): void
        
        function showSpellCircle(code: string): void;
        
        function hideSpellCircle(code: string): void;
    }
}

declare namespace NimbusArcane {
    interface SpellCircle {
        code: string;
        texture: string;
        element?: JQuery;
        textElement?: JQuery;
        imageElement?: JQuery;
    }
}