﻿import TomeOfMagic from "../tomeOfMagic";
import Chapters, {Chapter} from "./index";

class Front implements Chapter {
    static readonly id = "front";
    readonly id = Front.id;
    readonly langId = "";
    private page = 0;
    private pages: Array<[JQuery, JQuery]> = [];
    private translationCalls: Array<Array<string>> = [];
    public constructor() {
    }
    
    public init() {
        this.translationCalls.push([]);
        let mainPage = $("<div>");
        let title = $("<h1 style='text-align: center' data-lang='nimbusFox.nimbusArcane.title'>");

        mainPage.append(title);

        mainPage.append(
            $("<div style='font-size: 16px' data-lang='nimbusFox.nimbusArcane.description.1'>"),
            $("<br/>"),
            $("<div style='font-size: 16px' data-lang='nimbusFox.nimbusArcane.description.2'>")
        )

        this.translationCalls[0].push("nimbusFox.nimbusArcane.title");
        this.translationCalls[0].push("nimbusFox.nimbusArcane.description.1");
        this.translationCalls[0].push("nimbusFox.nimbusArcane.description.2");

        let menuPage = $("<div class='indexMenu'>");

        for (let chapter of Chapters.getChapters()) {
            if (chapter.id === this.id) {
                continue;
            }

            menuPage.append($(`<div data-lang="${chapter.langId}">`).text(chapter.id).click(() => {
                TomeOfMagic.showChapter(chapter.id);
            }));

            this.translationCalls[0].push(chapter.langId);
        }

        this.pages[0] = [
            mainPage,
            menuPage
        ];
    }
    
    public show() {
        this.pages[this.page][0].appendTo(TomeOfMagic.leftPage);
        this.pages[this.page][1].appendTo(TomeOfMagic.rightPage);
    }
    
    public updateLang() {
        for (let i = 0; i < this.translationCalls[this.page].length; i++) {
            getLanguageString(this.translationCalls[this.page][i]);
        }
    }

    canTurnBack(): boolean {
        return false;
    }

    canTurnForward(): boolean {
        return false;
    }

    showPageButtons(): boolean {
        return false;
    }

    turnBack(): void {
    }

    turnForward(): void {
    }
}

export default Front;
