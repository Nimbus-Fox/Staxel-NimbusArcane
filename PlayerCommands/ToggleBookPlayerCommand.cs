﻿using NimbusFox.KitsuneToolBox.PlayerCommands;
using NimbusFox.NimbusArcane.Item;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.NimbusArcane.PlayerCommands {
    public class ToggleBookPlayerCommand : PlayEmoteCommand {
        public new const string KindCode = "nimbusFox.nimbusArcane.playerCommand.toggleBook";

        public override string Kind() {
            return KindCode;
        }

        public override void Invoke(PlayerEntityLogic logic, Entity entity, Blob extendedActionBlob, Timestep timestep,
            EntityUniverseFacade facade) {
            base.Invoke(logic, entity, extendedActionBlob, timestep, facade);
            if (logic.Inventory().ActiveItem().IsNull()) {
                return;
            }

            if (logic.Inventory().ActiveItem().Item is TomeOfMagicItem tome) {
                if (extendedActionBlob.Contains("open")) {
                    var open = extendedActionBlob.GetBool("open");

                    if (open) {
                        tome.OpenBook();
                    } else {
                        tome.CloseBook();
                    }
                }
            }
        }
    }
}