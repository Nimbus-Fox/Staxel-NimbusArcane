﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusArcane.Database.Item;
using Plukit.Base;
using Staxel;
using Staxel.Browser;

namespace NimbusFox.NimbusArcane.Database {
    public class SpellCircleDatabase {
        private readonly Dictionary<string, SpellCircle> _circles = new Dictionary<string, SpellCircle>();

        internal void Initialize() {
            Deinitialize();

            foreach (var resource in GameContext.AssetBundleManager.FindByExtension(".spellCircle")) {
                using (var stream = GameContext.ContentLoader.ReadStream(resource)) {
                    var config = BlobAllocator.Blob(true);

                    config.LoadJsonStream(stream);
                
                    AddNewSpellCircle(new SpellCircle(config));
                }
            }
        }

        internal void Deinitialize() {
            _circles.Clear();
        }

        public bool TryFetchSpellCircle(string code, out SpellCircle? spellCircle) {
            spellCircle = null;

            if (_circles.ContainsKey(code)) {
                spellCircle = _circles[code];
                return true;
            }

            return false;
        }

        public void AddNewSpellCircle(SpellCircle spellCircle) {
            _circles.Add(spellCircle.Code, spellCircle);
        }

        internal void SendCirclesToUI(BrowserRenderSurface surface) {
            var blob = BlobAllocator.Blob(true);

            foreach (var circle in _circles.Values) {
                blob.SetString("code", circle.Code);
                blob.SetString("texture", circle.Texture);
                
                surface.CallFunction("nimbusArcane.spellCircles.addSpellCircle", blob.ToString());
                surface.CallFunction("nimbusArcane.spellCircles.showSpellCircle", circle.Code);
            }
            
            surface.CallFunction("nimbusArcane.spellCircles.setSelectedCircle", _circles.Keys.First());
        }
    }
}
