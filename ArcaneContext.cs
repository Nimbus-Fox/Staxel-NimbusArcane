﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.NimbusArcane.Database;

namespace NimbusFox.NimbusArcane {
    public static class ArcaneContext {
        public static readonly SpellCircleDatabase SpellCircleDatabase = new SpellCircleDatabase();

        internal static void Initialize() {
            SpellCircleDatabase.Initialize();
        }

        internal static void Deinitialize() {
            SpellCircleDatabase.Deinitialize();
        }
    }
}
