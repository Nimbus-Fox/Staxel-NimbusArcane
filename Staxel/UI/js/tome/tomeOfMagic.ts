﻿import Chapters, {Chapter} from "./chapters"
import Front from "./chapters/front";

module TomeOfMagic {
    let chapters: {[key: string] : Chapter} = {};
    
    let chapterIndex = "";
    
    let tome = $("#TomeOfMagic_Container");
    export const leftPage = $("#leftPage");
    export const rightPage = $("#rightPage");
    export function init() {
        for (let chapter of Chapters.getChapters()) {
            chapter.init();
            chapters[chapter.id] = chapter;
        }
        
        TomeOfMagic.init = undefined;

        showFrontPage();
    }
    
    export function clear() {
        leftPage.empty();
        rightPage.empty();
    }
    
    export function showFrontPage() {
        showChapter(Front.id);
    }
    
    export function showChapter(key: string) {
        clear();
        chapterIndex = key;
        
        chapters[chapterIndex].show();
        chapters[chapterIndex].updateLang();
    }
    
    export function showTome() {
        tome.show();
    }
    
    export function hideTome() {
        tome.hide();
    }
    
    tome.hide();
}

export default TomeOfMagic

stxl["nimbusArcane"]["tomeOfMagic"] = TomeOfMagic;