﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Items.V1;
using NimbusFox.KitsuneToolBox.Items.V1.Builders;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Crafting;
using Staxel.Entities;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Player;

namespace NimbusFox.NimbusArcane.Entities.SpellCircle {
    public class SpellCircleEntityLogic : EntityLogic {
        private readonly Entity _entity;
        private Blob _constructor;
        protected bool NeedStore { get; private set; }
        protected string ChalkCode { get; private set; }
        private bool _remove;
        private Vector3D Min = Vector3D.Zero;
        private Vector3D Max = Vector3D.Zero;
        public Vector3I Origin { get; private set; } = Vector3I.Zero;
        protected readonly List<Entity> Items = new List<Entity>();
        protected const float RotationSpeed = 0.0003f;
        public Database.Item.SpellCircle? SpellCircle { get; private set; }
        private bool _isValidCraft;
        private bool _revalidateCraft;
        private ReactionDefinition _reaction;

        private readonly List<(string code, string kind, int count)> _craftList =
            new List<(string code, string kind, int count)>();

        private short _amountToCraft;

        internal SpellCircleEntityLogic(Entity entity) {
            _entity = entity;
        }

        public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (Items.RemoveAll(item =>
                    item.Removed || item.Physics == null ||
                    !((UnsealedItemEntityLogic)item.Logic).PickedUpBy.IsNull()) >= 1) {
                _revalidateCraft = true;
            }

            entityUniverseFacade.ForAllEntitiesInRange(Origin.ToTileCenterVector3D(), 2.5f, entity => {
                if (entity.Logic is ItemEntityLogic) {
                    if (entity.Physics.Position.IsBetween(Min, Max) && !Items.Contains(entity)) {
                        var stack = entity.ItemEntityLogic.GetPrivateFieldValue<ItemStack>("Stack");
                        var ent = UnsealedItemEntityBuilder.SpawnDroppedItem(_entity, entityUniverseFacade, stack,
                            entity.Physics.Position, Vector3D.Zero, Vector3D.Zero, SpawnDroppedFlags.NoAutoPickup);

                        ((UnsealedItemEntityLogic)ent.Logic).IgnoreAutoPickup = true;
                        Items.Add(ent);
                        entityUniverseFacade.RemoveEntity(entity.Id);
                        _revalidateCraft = true;
                    }
                }

                if (entity.Logic is UnsealedItemEntityLogic) {
                    if (entity.Physics.Position.IsBetween(Min, Max) && !Items.Contains(entity)) {
                        Items.Add(entity);
                        _revalidateCraft = true;
                    }
                }
            });
        }

        public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_revalidateCraft) {
                _revalidateCraft = false;
                _isValidCraft = false;
                _reaction = null;
                _amountToCraft = 0;
                _craftList.Clear();
                if (SpellCircle == null) {
                    RemoveCircle(_entity, entityUniverseFacade);
                    return;
                }

                foreach (var item in Items) {
                    var stack = ((UnsealedItemEntityLogic)item.Logic).GetPrivateFieldValue<ItemStack>("Stack");
                    (string code, string kind, int count) def;
                    if (_craftList.Any(x => x.code == stack.Item.GetItemCode())) {
                        def = _craftList.First(x => x.code == stack.Item.GetItemCode());

                        _craftList.Remove(def);

                        def.count += stack.Count;

                        _craftList.Add(def);
                        continue;
                    }

                    def = (stack.Item.GetItemCode(), stack.Item.Kind, stack.Count);
                    _craftList.Add(def);
                }

                var targets = SpellCircle?.Reactions.Where(x => x.Reagents.Count == _craftList.Count).ToList();

                if (!targets.Any()) {
                    NeedToStore();
                    return;
                }

                foreach (var target in targets) {
                    var craft = true;
                    short amount = 0;
                    foreach (var reagent in target.Reagents) {
                        var item = _craftList.FirstOrDefault(x =>
                            x.code == reagent.ItemBlob.GetString("code") &&
                            x.kind == reagent.ItemBlob.GetString("kind", "staxel.item.CraftItem") &&
                            x.count >= reagent.Quantity);

                        if (item == default((string, string, short))) {
                            craft = false;
                            amount = 0;
                            break;
                        }

                        if (amount == 0) {
                            amount = (short)(item.count / reagent.Quantity);
                        } else {
                            var am = (short)(item.count / reagent.Quantity);
                            amount = amount <= am
                                ? amount
                                : am;
                        }
                    }

                    if (craft) {
                        _isValidCraft = true;
                        _reaction = target;
                        _amountToCraft = amount;
                        NeedToStore();
                    }
                }
            }
        }

        public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
            if (_remove) {
                entityUniverseFacade.RemoveEntity(_entity.Id);
                return;
            }

            if (Items.Count == 0) {
                return;
            }

            if (Items.Count == 1) {
                Items[0].Physics.ForcedPosition(Origin.ToTileCenterVector3D() + new Vector3D(0, 2, 0));
                return;
            }

            var angle = MathHelper.ToDegrees((float)(new TimeSpan(DateTime.UtcNow.Ticks).TotalMilliseconds *
                RotationSpeed % (2 * Math.PI)));
            var addition = 360f / Items.Count;
            var index = 0;

            foreach (var item in Items) {
                var currentAngle = angle + (addition * index);

                if (currentAngle >= 360) {
                    currentAngle -= 360;
                }

                currentAngle = MathHelper.ToRadians(currentAngle);

                var offset = new Vector3D(Math.Sin(currentAngle), 0, Math.Cos(currentAngle)) * 2f;

                item.Physics.ForcedPosition(Origin.ToTileCenterVector3D() + offset + new Vector3D(0, 2, 0));

                index++;
            }
        }

        public override void Store() {
            if (!NeedStore) {
                return;
            }

            NeedStore = false;
            if (SpellCircle != null) {
                _entity.Blob.SetString("spellCircle", SpellCircle?.Code);
            }

            if (_reaction != null) {
                _entity.Blob.SetString("reaction", _reaction.Code);
            }

            _entity.Blob.SetBool("validCraft", _isValidCraft);
            _entity.Blob.FetchBlob("origin").SetVector3I(Origin);
        }

        public override void Restore() {
            if (_entity.Blob.Contains("spellCircle")) {
                if (ArcaneContext.SpellCircleDatabase.TryFetchSpellCircle(_entity.Blob.GetString("spellCircle"),
                        out var spellCircle)) {
                    SpellCircle = spellCircle;
                }
            }

            if (_entity.Blob.Contains("reaction")) {
                _reaction = GameContext.ReactionDatabase.GetReactionDefinition(_entity.Blob.GetString("reaction"));
            }

            if (_entity.Blob.Contains("validCraft")) {
                _isValidCraft = _entity.Blob.GetBool("validCraft");
            }

            if (_entity.Blob.Contains("origin")) {
                Origin = _entity.Blob.FetchBlob("origin").GetVector3I();
            }
        }

        public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
            _constructor = arguments.Clone();

            _entity.Physics.ForcedPosition(arguments.FetchBlob("location").GetVector3I().ToTileCenterVector3D() +
                                           new Vector3D(0, -0.225, 2));

            Origin = arguments.FetchBlob("location").GetVector3I();

            Min = Origin.ToVector3D() - new Vector3D(2, 0, 2);
            Max = Origin.ToVector3D() + new Vector3D(3, 1, 3);

            var shape = Shape.MakeBox(new Vector3D(-2.5, 0, -4.5), new Vector3D(5, 0, 5));
            shape.Kind = ShapeKind.Box;

            _entity.Physics.SetNonDefaultBoundingBox(shape, shape, shape);
            _entity.Physics.HasPhysics = true;
            _entity.Physics.NeedsStorage();

            ChalkCode = arguments.GetString("chalkCode");

            if (ArcaneContext.SpellCircleDatabase.TryFetchSpellCircle(arguments.GetString("spellCircle"),
                    out var spellCircle)) {
                SpellCircle = spellCircle;
            } else {
                RemoveCircle(_entity, entityUniverseFacade);
            }

            NeedToStore();
        }

        public override void Bind() { }

        public override bool Interactable() {
            return true;
        }

        public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (entity.Logic is PlayerEntityLogic == false) {
                return;
            }

            if (alt.DownClick) {
                if (_isValidCraft) {
                    var canCraft = false;
                    ItemStack? toConsume = null;
                    if (_reaction.Tools.Any()) {
                        var holding = entity.PlayerEntityLogic.Inventory().ActiveItem();

                        foreach (var tool in _reaction.Tools) {
                            if (holding.Item.GetItemCode() == tool.ItemBlob.GetString("code", "") &&
                                holding.Count == tool.Quantity) {
                                canCraft = true;
                                if (_reaction.ConsumeTool) {
                                    toConsume = new ItemStack(GameContext.ItemDatabase.SpawnItem(tool.ItemBlob, null),
                                        tool.Quantity);
                                }

                                break;
                            }
                        }
                    } else {
                        canCraft = true;
                    }

                    if (!canCraft) {
                        var blb = BlobAllocator.Blob(true);
                        blb.SetString("code", "nimbusFox.nimbusArcane.notifications.invalidTool");
                            
                        entity.PlayerEntityLogic.ShowNotification(GameContext.NotificationDatabase.CreateNotification(blb, facade.Step, NotificationParams.EmptyParams, true));
                            
                        Blob.Deallocate(ref blb);
                        return;
                    }

                    if (_reaction.ConsumeTool && toConsume != null && !entity.PlayerEntityLogic.CreativeModeEnabled()) {
                        entity.Inventory.RemoveItem(toConsume.Value);
                    }

                    var result = GetResults();
                    for (var i = 0; i < _amountToCraft; i++) {
                        foreach (var item in result) {
                            ItemEntityBuilder.SpawnDroppedItem(facade, item,
                                Origin.ToTileCenterVector3D() + Vector3D.UnitY, Vector3D.UnitY, Vector3D.Zero,
                                SpawnDroppedFlags.None);
                        }
                    }

                    foreach (var item in Items) {
                        facade.RemoveEntity(item.Id);
                    }

                    return;
                }

                RemoveCircle(entity, facade);
            }
        }

        public override bool CanChangeActiveItem() {
            return false;
        }

        public override Heading Heading() {
            return default;
        }

        public override bool IsPersistent() {
            return true;
        }

        public override void StorePersistenceData(Blob data) {
            data.FetchBlob("constructor").AssignFrom(_constructor);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            if (data.Contains("constructor")) {
                Construct(data.FetchBlob("constructor"), facade);
            }
        }

        public override bool IsCollidable() {
            return false;
        }

        protected void NeedToStore() {
            NeedStore = true;
        }

        public override string AltInteractVerb() {
            return _isValidCraft ? "nimbusfox.nimbusArcane.verb.Craft" : "nimbusfox.nimbusArcane.verb.Remove";
        }

        public void RemoveCircle(Entity entity, EntityUniverseFacade facade) {
            if (entity.Logic is PlayerEntityLogic logic) {
                if (!logic.CreativeModeEnabled()) {
                    ItemEntityBuilder.SpawnDroppedItem(entity, facade,
                        new ItemStack(HelperFunctions.MakeItem(ChalkCode), 1), _entity.Physics.Position, Vector3D.UnitY,
                        Vector3D.Zero, SpawnDroppedFlags.AttemptPickup);
                }
            }

            _remove = true;
        }

        public IReadOnlyList<ItemStack> GetResults() {
            return _isValidCraft
                ? (from result in _reaction.Results
                    let item = GameContext.ItemDatabase.SpawnItem(result.ItemBlob, Staxel.Items.Item.NullItem)
                    select new ItemStack(item, result.Quantity)).ToList()
                : null;
        }
    }
}