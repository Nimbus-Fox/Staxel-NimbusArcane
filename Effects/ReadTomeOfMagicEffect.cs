﻿using NimbusFox.KitsuneToolBox;
using NimbusFox.KitsuneToolBox.PlayerCommands;
using NimbusFox.NimbusArcane.PlayerCommands;
using Plukit.Base;
using Staxel;
using Staxel.Browser;
using Staxel.Draw;
using Staxel.Effects;
using Staxel.Input;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Rendering;

namespace NimbusFox.NimbusArcane.Effects {
    public class ReadTomeOfMagicEffect : IEffect {
        private bool _completed;

        public ReadTomeOfMagicEffect(Blob data, EntityUniverseFacade facade) {
            if (!facade.TryGetEntity(new EntityId(data.GetLong("playerEntity")), out _) || ClientContext.PlayerFacade.IsInMapMode || ClientContext.PlayerFacade.IsInSelfieMode) {
                _completed = true;
                return;
            }
            
            ToolBoxContext.BrowserManagerV1.PrepareUIControl();
            ClientContext.WebOverlayRenderer.Call("nimbusArcane.tomeOfMagic.showTome");

            var blob = BlobAllocator.Blob(true);
            blob.SetString("action", ToggleBookPlayerCommand.KindCode);
            blob.SetString("code", "staxel.emote.Read.loop");
            blob.SetBool("open", true);
            ClientContext.OverlayController.AddCommand(blob);
        }
        
        public void Dispose() {
            
        }

        public bool Completed() {
            return _completed;
        }

        public void Render(Entity entity, EntityPainter painter, Timestep renderTimestep, DeviceContext graphics, ref Matrix4F matrix,
            Vector3D renderOrigin, Vector3D position, RenderMode renderMode) {
            if (_completed) {
                return;
            }
            
            if (renderMode != RenderMode.Normal) {
                return;
            }

            if (!ClientContext.PlayerFacade.IsLocalPlayer(entity) || ClientContext.OverlayController.IsOpen()) {
                return;
            }

            if (ClientContext.InputSource.IsGameDown(GameLogicalButton.Esc) || (ClientContext.InputSource.IsAnyGamePadDownClick(out var axis) && axis == GamePadAxis.B)) {
                ToolBoxContext.BrowserManagerV1.ReleaseUIControl();
                ClientContext.WebOverlayRenderer.Call("nimbusArcane.tomeOfMagic.hideTome");

                var blob = BlobAllocator.Blob(true);
                blob.SetString("action", ToggleBookPlayerCommand.KindCode);
                blob.SetString("code", "staxel.emote.Read.end");
                blob.SetBool("open", false);
                ClientContext.OverlayController.AddCommand(blob);
                
                _completed = true;
            }
        }

        public void Stop() {
            
        }

        public void Pause() {
            
        }

        public void Resume() {
            
        }
    }
}