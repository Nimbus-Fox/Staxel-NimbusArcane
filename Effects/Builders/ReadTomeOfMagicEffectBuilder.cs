﻿using Plukit.Base;
using Staxel.Effects;
using Staxel.Logic;

namespace NimbusFox.NimbusArcane.Effects.Builders {
    public class ReadTomeOfMagicEffectBuilder : IEffectBuilder {
        public const string KindCode = "nimbusFox.nimbusArcane.effects.showSpellCircles";
        public void Dispose() {
            
        }

        public void Load() {
            
        }

        public string Kind() {
            return KindCode;
        }

        public IEffect Instance(Timestep step, Entity entity, EntityPainter painter, EntityUniverseFacade facade, Blob data,
            EffectDefinition definition, EffectMode mode) {
            return new ReadTomeOfMagicEffect(data, facade);
        }
    }
}