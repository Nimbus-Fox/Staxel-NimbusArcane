﻿using NimbusFox.KitsuneToolBox.Interfaces;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.NimbusArcane.Item.Builders {
    public class TomeOfMagicItemBuilder : IItemBuilderHelperV1 {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new ItemRenderer();
        }
        
        public Staxel.Items.Item Build(Blob blob, ItemConfiguration configuration, Staxel.Items.Item spare) {
            if (spare is TomeOfMagicItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var tome = new TomeOfMagicItem(this);
            tome.Restore(configuration, blob);
            return tome;

        }

        public static string KindCode() {
            return "nimbusFox.nimbusArcane.item.tomeOfMagic";
        }

        public string Kind() {
            return KindCode();
        }

        public ItemRenderer Renderer { get; private set; }
    }
}